# README #

### What is this repository for? ###

* This is an ADF dumper script for KryoFlux
* The script can do single files or it can run in loop mode for dumping hundreds of disks in a row.
* The workflow has been optimized according to lessons learned after having dumped thousands of Amiga disks.

### How do I get set up? ###

* You need to have dtc set up so that it and its libs are in the path
* You need xdftool in your path, get it from https://github.com/cnvogelg/amitools.git
* Bash 4 or newer 
* Coreutils
* Sed
* Grep
* The Zip archiver
* Procps
* Awk

### The workflow ###

* If you want an image of a single disk, just say ./adf.sh -n imagename. You will end up with imagename.adf in dump_disks and if you wish, imagename.zip (containing the raw dump) in dump_raw. However the real point of the script is in the continuous mode.
* It is assumed that you're dumping a single person's disks per working directory. The script does not reuse disk IDs (four digits in the beginning of filenames) and it makes an educated guess about what ID to start/continue from.
* The script always uses the directory it's run from as its working directory. There is no warning about this, but the script is rather neat; if things run smoothly, it only leaves 2 directories and one file behind in the current dir.
* You can override the starting ID with the -c parameter if you don't want to select it interactively.
* If you would like to store and archive the raw tracks, you should specify the -a parameter. The default behaviour is to only make an adf file.
* While dtc is running, you can break back to the script with ^C in case you want to quickly clean and retry a disk.
* After the starting ID has been selected, the loop begins.

1. The disk is read into an image file. You can retry the same ID multiple times. If a failure happened and you want to continue, the temporary directory for this ID is left behind for further analysis. Upon successful imaging, the image (ID.adf) gets moved from the temp directory into the dump_disks directory.
2. The raw files are packed away into dump_raw/ID.zip and the temp directory is removed.
3. The script tries to automatically determine a suitable filename for the image. If the disk is not DOS, or the root block is mangled by a virus, then NDOS is offered. When dumping multiple subsequent NDOS disks, the script remembers the previous name, and if you had a number in the end, it will try to autoincrement the number for you to make archiving multi-disk releases easier. To keep the ID numbers between the adfs and raw zips in sync, you cannot edit the ID part of the name.
4. A session log is appended to session.log. DOS disks get their directory listed, NDOS disks get an ASCII bootblock dump listed. The bootblock dump is wrapped at 64 characters, as most boot block ascii art is written for that line length.

* After you're done with one person's disks, you should archive and remove dump_disks, dump_raw, and session.log so that the next person's disks will not get mixed up with the previous images.
