#!/bin/bash
#Copyright (C) 2014, Ville Jouppi <jope@jope.fi>
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright owner nor the names of contributors to this software may be used to endorse or promote products derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#set -x

dump () {
  if [ -d "$T" ];then
    read -e -p "The temp dir for $T exists, kill it? [Y/n]? " -i "Y" RES
    RES=$(echo "$RES" | tr 'Y' 'y' | cut -c1)
    [ "$RES" = "y" ] && rm -rf "$T"
  fi

  while true; do
    READERROR=0
    echo "Dumping $T..."
    trap 'pkill dtc' SIGINT
    if [ $ARCHIVE -eq 1 ]; then
      dtc -p -f"$T/$T" -i0 -f"$T/$T.adf" -i5 -l8 -t$RETRY 2> >(tee $T.log)
    else
      dtc -p -f"$T/$T.adf" -i5 -l8 -t$RETRY 2> >(tee $T.log)
    fi
    trap - SIGINT

    if [ ! -f "$T/$T.adf" ]; then
      echo "ERROR: No adf generated."
      ERROR=1
    fi

    if [ $(du "$T/$T.adf" | cut -f1 -d"	") -eq 0 ];then
      echo "ERROR: Null size adf generated."
      ERROR=1
    fi

    grep "Read" $T.log
    [ $? -eq 0 ] && READERROR=1
    rm $T.log

    if [ $ERROR -eq 1 ]; then
      read -e -p "Reread disk $T? [Y/n/q]? " -i "Y" RES
    else
      read -e -p "Reread disk $T? [y/N/q]? " -i "N" RES
    fi
    RES=$(echo "$RES"|cut -c1)
    case $RES in
      y|Y) rm -rf "$T" && ERROR=0 ;;
      q|Q) exit 0 ;;
      *) break ;;
    esac
  done

  if [ $ERROR -eq 0 ]; then 
    echo "Moving $T.adf to dump_disks..."
    mv "$T/$T.adf" "dump_disks/"
  else
    echo "Temp dir $T left intact, moving on to the next disk."
    MOVEALONG=1
  fi
}

packraw () {
  if [ $ARCHIVE -eq 1 ]; then
    echo "Packing raw files for $T in the background."
    (zip -r9 "dump_raw/$T.zip" "$T" > /dev/null && rm -rf "$T") &
  else
    (rm -rf "$T") &
  fi
}

renameadf () {
  ROOTBLK=$(xdftool dump_disks/$T.adf root show 2>/dev/null)
  echo "$ROOTBLK" | grep FSError 2>&1 >/dev/null
  if [ $? -eq 0 ]; then
    # Let's handle NDOS
    if [ $WASNDOS -eq 1 ];then
      NAME="$OLDNAME"
      # here we do a little regexp mangling to auto increment the number at
      # the end of an NDOS disk's filename
      RES=$(echo ${NAME: -1} | grep '[[:digit:]]')
      if [ -n "$RES" ]; then
        ANYNUM=$(echo $NAME|awk '{ gsub(/[^0-9]/," ");print $NF}')
        NAME=${NAME%$ANYNUM}$(expr $ANYNUM + 1)
      fi
    else
      NAME="NDOS"
    fi
    # It's nice to remember the name for the previous NDOS disk in case
    # it's a multi disk prod.
    WASNDOS=1
    # Show the bootblock to aid in choosing a name
    dd if=dump_disks/$T.adf bs=1024 count=1 status=none | tr -c '[[:print:]]' '.'|fold -w64
    echo ""
  else
    NAME=$(echo "$ROOTBLK" | env LANG=C grep disk.name | cut -c13-|tr " " "_"|tr -cd $ACCEPTCHARS)
    OLDNAME="NDOS"
    WASNDOS=0
    # Show the root dir to aid in choosing a name
    xdftool dump_disks/$T.adf list /
  fi

  read -e -p "Disk name: $COUNTER-" -i "$NAME" NAME
  NAME=$(echo "$NAME"|tr " " "_"|tr -cd $ACCEPTCHARS)
  NAME=${NAME%.adf}
  if [ -n "$NAME" ]; then
    echo "Renaming to $COUNTER-$NAME.adf"
    mv dump_disks/$T.adf dump_disks/$COUNTER-$NAME.adf
    T="$COUNTER-$NAME"
    [ $WASNDOS -eq 1 ] && OLDNAME="$NAME"
  else
    echo "Keeping $COUNTER.adf"
  fi
} 

sessionlog () {
  echo ------------start----------------- >> session.log
  echo $T.adf >> session.log
  echo ---------------------------------- >> session.log
  if [ $WASNDOS -eq 1 ]; then
    # NDOS disks don't have a meaningful root block, so let's dump the
    # bootblock instead. We wrap at 64 chars just like most Amiga viewers do.
    dd if=dump_disks/$T.adf bs=1024 count=1 status=none | tr -c '[[:print:]]' '.'|fold -w64 >> session.log
    echo "">> session.log
  else
    # DOS disks get the dir listing
    xdftool dump_disks/$T.adf list >> session.log
  fi
  echo ------------end------------------- >> session.log
}

dumpexists () {
  if [ -f "dump_disks/$T.adf" ];then
    read -e -p "dump_disks/$T.adf exists, kill? [Y/n]? " -i "Y" RES
    RES=$(echo "$RES" | tr 'Y' 'y' | cut -c1)
    if [ "$RES" = "y" ];then
       rm dump_disks/"$T.adf"
    else
       echo "Aborting."
       ERROR=1
    fi
  fi
}

numberexists () {
  [ -f "dump_disks/$COUNTER.adf" ] && ERROR=1 && return
  [ -f dump_disks/$COUNTER-*.adf ] && ERROR=1 && return
}

padcounter () {
  [ ${#COUNTER} -ge 4 ] && return
  COUNTER=$(printf %04d "$COUNTER")
}

# begin
COUNTER=-1
RETRY=5
T=""
WASNDOS=0
ACCEPTCHARS='[[:alnum:]].\-_'
MOVEALONG=0
ARCHIVE=0

# Read the options
if [ $# -gt 0 ];then
  OPTS=$(getopt -o har:n:c: -- "$@")
  [ $? -gt 0 ] && exit $?
  eval set -- "$OPTS"
  while true ; do
    case "$1" in
      -n) T="$(echo $2|tr " " "_"|tr -cd $ACCEPTCHARS)" ; T=${T%.adf} ; shift 2 ;;
      -r) RETRY="$2" ; shift 2 ;;
      -h) echo "Usage: $0 OPTION...
Dumps ADF and raw images from a 3.5\" floppy connected to a KryoFLux.

Omit -n and -c to go into interactive counter mode.

Optional parameters:
  -r RETRYCOUNT  Numeric retry value (default is 5)
  -n FILENAME    Make a single ADF with this name
  -c COUNT       Image counter seed
  -a             Archive raw tracks (default is adf only)" ; exit 1 ;;
      -c) COUNTER=$(echo "$2"|tr -cd '[[:digit:]]') ; shift 2 ;;
      -a) ARCHIVE=1 ; shift ;;
      --) shift ; break ;;
    esac
  done
fi

# A bit of setup
RETRY=$(echo "$RETRY"|tr -cd '[[:digit:]]')
[ -d "dump_raw" ] || mkdir dump_raw
[ -d "dump_disks" ] || mkdir dump_disks
[ -f "session.log" ] || touch session.log

echo "Using retry value: $RETRY."
echo -n "Archiving of raw tracks: "
[ $ARCHIVE -eq 0 ] && echo "off."
[ $ARCHIVE -eq 1 ] && echo "on."
echo ""

if [ -n "$T" ];then
  # Make a single disk image and exit since user selected -n
  echo "Creating $T.adf"
  ERROR=0
  dumpexists
  [ $ERROR -eq 1 ] && exit 1
  dump
  [ $ERROR -eq 1 ] && exit 1
  packraw
  [ $ERROR -eq 0 ] && sessionlog
  exit 0
fi

# No filename, so we want counter mode.
if [ $COUNTER -lt 0 ] ;then
  # Interactive counter mode.
  echo "Latest dumps:"
  ls dump_disks|sort -u|grep ^[0-9]|tail -10
  LASTDISK=$(ls dump_disks|sort -u|grep ^[0-9]|tail -1|cut -c1-4|tr -cd '[[:digit:]]')

  if [ -z "$LASTDISK" ];then
    COUNTER="0001"
  else
    COUNTER=$(expr $LASTDISK + 1)
    padcounter
  fi
 
  echo ""
  read -e -p "Start from: " -i "$COUNTER" COUNTER
  COUNTER=$(echo "$COUNTER"|tr -cd '[[:digit:]]')
  [ -z "$COUNTER" ] && COUNTER="0001" && echo "Couldn't parse your input, starting from 0001"
fi 

padcounter

read -p "Insert disk number $COUNTER and press enter to begin.."

while true; do
  T="$COUNTER"
  ERROR=0
  numberexists
  [ $ERROR -eq 1 ] && echo "Skipping $COUNTER."
  [ $ERROR -eq 0 ] && dump
  [ $ERROR -eq 0 ] && packraw
  [ $ERROR -eq 0 ] && renameadf
  [ $ERROR -eq 0 ] && sessionlog 
  COUNTER=$(expr $COUNTER + 1)
  padcounter
  [ $MOVEALONG -eq 1 ] && ERROR=0 && MOVEALONG=0
  [ $ERROR -eq 0 ] && while true ;do
                        read -e -p "Begin to image disk $COUNTER or Quit [Y/q]? " -i "Y" RES
                        RES=$(echo "$RES" | cut -c1)
                        case $RES in 
                          Y|y) break ;;
                          Q|q) exit 0 ;;
                        esac
                      done
done
